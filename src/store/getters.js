import { MAIN_COLOR } from '@/constant'
import { getItem } from '@/utils/storage'
import { generateColors } from '@/utils/theme'
const getters = {
  tagsViewList: state => state.app.tagsViewList,
  mainColor: (state) => state.theme.mainColor,
  language: (state) => state.app.language,
  sidebarOpened: (state) => state.app.sidebarOpened,
  cssVar: (state) => {
    return {
      ...state.theme.variables,
      ...generateColors(getItem(MAIN_COLOR))
    }
  },
  userInfo: (state) => state.user.userInfo,
  /**
   * @returns true 表示已存在用户信息
   */
  hasUserInfo: (state) => {
    return JSON.stringify(state.user.userInfo) !== '{}'
  },
  token: (state) => {
    let token = state.user.token
    if (!token) {
      token = getItem('token')
    }
    return token
  }
}
export default getters
